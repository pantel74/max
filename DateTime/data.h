#ifndef DATA_H
#define DATA_H

#include <QString>   //если для Qt, то там можно использовать для строк класс QString

//Обычно название класса лучше писать с заглавной буквы
class Data
{
public:   
        Data();
		
		int nDayInYear(); //номер дня в году (перенесено из private в public), теперь number() - не нужна
		
		
        inline void setData( int day, int month, int year){
			_day = day;
			_month = month;
			_year = year;
		}
		
		//Убрал inline, иначе тут же должно быть тело функции
		//getData -> toString()
        QString toString();		
		        
		//Сезон года
        QString Season();
		
		//Високосность года 
		bool isLeapYear();
		//можно сделать и статической, только для статической надо параметр
		static bool isLeapYear(int year); //Прошу тут добавить код реализации !!!!!!!!!!!!!!!!!!!!!!!!!
		
		//Прошу добавить код определения кол-ва дней до другой даты !!!!!!!!!
		int getNumDaysTo(Data d);
		
private:
        int _day, _month, _year;  //переменным-членам класса лучше добавить черточку вначале
        
		
};

#endif // DATA_Hpublic:

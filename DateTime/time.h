#ifndef TIME_H
#define TIME_H

//Обычно название класса лучше писать с заглавной буквы
class Time
{
private:
            float sec, hour, min, k;
            float nSec();
    public:
            inline void setTime( float hour2, float min2, float sec2);
            inline void getTime();
            inline float Number2();
};

#endif // TIME_H
